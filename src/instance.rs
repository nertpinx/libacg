use aws_sdk_ec2::model::{Instance, InstanceStateName};

use crate::helpers::*;
use crate::*;

#[derive(Clone)]
pub struct AcgInstance(pub(crate) Instance);

impl AcgInstance {
    pub fn new(instance: Instance) -> Self {
        Self(instance)
    }

    pub fn from_handle(handle: *mut AcgInstance) -> AcgResult<Self> {
        if handle.is_null() {
            Err(AcgError::InvalidPointer)
        } else {
            Ok(Box::leak(unsafe { Box::from_raw(handle) }).clone())
        }
    }

    pub fn get_state(&self) -> AcgResult<AcgInstanceState> {
        Ok(self
            .0
            .state()
            .ok_or(AcgError::InvalidInstance)?
            .name()
            .ok_or(AcgError::InvalidInstance)?
            .into())
    }
}

impl FromC for &mut AcgInstance {
    type Target = AcgInstance;

    fn try_convert(self) -> AcgResult<Self::Target> {
        AcgInstance::from_handle(self)
    }
}

#[repr(C)]
#[non_exhaustive]
pub enum AcgInstanceState {
    Pending,
    Running,
    ShuttingDown,
    Stopped,
    Stopping,
    Terminated,
    Unknown,
}

impl ToC for AcgInstanceState {
    type Target = AcgInstanceState;

    fn try_convert(self) -> AcgResult<Self::Target> {
        Ok(self)
    }
}

impl From<&InstanceStateName> for AcgInstanceState {
    fn from(value: &InstanceStateName) -> Self {
        match value {
            InstanceStateName::Pending => AcgInstanceState::Pending,
            InstanceStateName::Running => AcgInstanceState::Running,
            InstanceStateName::ShuttingDown => AcgInstanceState::ShuttingDown,
            InstanceStateName::Stopped => AcgInstanceState::Stopped,
            InstanceStateName::Stopping => AcgInstanceState::Stopping,
            InstanceStateName::Terminated => AcgInstanceState::Terminated,
            _ => AcgInstanceState::Unknown,
        }
    }
}
