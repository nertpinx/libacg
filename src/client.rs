use std::sync::Arc;

use aws_config::meta::region::RegionProviderChain;
use aws_sdk_ec2::{Client, Region};

use crate::{helpers::FromC, *};

#[derive(Clone)]
pub struct Acg(Arc<AcgClient>);

impl Acg {
    pub(crate) fn new(region: Option<&str>) -> AcgResult<Self> {
        Ok(Self(Arc::new(AcgClient::new(region)?)))
    }
}

pub(crate) struct AcgClient {
    runtime: tokio::runtime::Runtime,
    region: String,
    client: Client,
}

impl AcgClient {
    pub fn new(region: Option<&str>) -> AcgResult<Self> {
        let runtime = tokio::runtime::Builder::new_current_thread()
            .enable_time()
            .enable_io()
            .build()?;
        let asdf = region.map(String::from).map(Region::new);
        let region_provider = RegionProviderChain::first_try(asdf).or_default_provider();

        let region = runtime
            .block_on(region_provider.region())
            .ok_or(AcgError::NoRegion)?;

        let config = runtime.block_on(aws_config::from_env().region(region_provider).load());
        let client = Client::new(&config);

        Ok(Self {
            runtime,
            region: region.to_string(),
            client,
        })
    }

    pub fn from_handle(handle: *mut Acg) -> AcgResult<Arc<Self>> {
        if handle.is_null() {
            Err(AcgError::InvalidPointer)
        } else {
            Ok(Box::leak(unsafe { Box::from_raw(handle) }).0.clone())
        }
    }

    pub fn get_region(&self) -> &str {
        &self.region
    }

    pub fn get_instance_by_id(&self, id: &str) -> AcgResult<AcgInstance> {
        let response = self
            .runtime
            .block_on(self.client.describe_instances().instance_ids(id).send())?;

        let list: Vec<_> = response
            .reservations
            .unwrap_or_default()
            .iter()
            .filter_map(|r| {
                r.instances
                    .as_ref()
                    .map(|i| i.iter().cloned().map(AcgInstance::new).collect::<Vec<_>>())
            })
            .flatten()
            .collect();

        list.get(0).ok_or(AcgError::NoSuchInstance).cloned()
    }

    pub fn get_instances(&self) -> AcgResult<Vec<AcgInstance>> {
        let response = self
            .runtime
            .block_on(self.client.describe_instances().send())?;

        let ret = response
            .reservations
            .unwrap_or_default()
            .iter()
            .filter_map(|r| {
                r.instances
                    .as_ref()
                    .map(|i| i.iter().cloned().map(AcgInstance::new).collect::<Vec<_>>())
            })
            .flatten()
            .collect();

        // let mut ret = vec![];

        // for r in reservations {
        //     if let Some(instances) = r.instances {
        //         for i in instances {
        //             ret.push(AcgInstance::new(i));
        //         }
        //     }
        // }

        //        let instances = instances.iter().map(AcgInstance::new).collect();

        Ok(ret)
    }

    pub fn start_instance(&self, inst: &AcgInstance) -> AcgResult<()> {
        let id = inst.0.instance_id().ok_or(AcgError::InvalidInstance)?;

        self.runtime
            .block_on(self.client.start_instances().instance_ids(id).send())?;

        Ok(())
    }

    pub fn stop_instance(&self, inst: &AcgInstance) -> AcgResult<()> {
        let id = inst.0.instance_id().ok_or(AcgError::InvalidInstance)?;

        self.runtime
            .block_on(self.client.stop_instances().instance_ids(id).send())?;

        Ok(())
    }

    pub fn terminate_instance(&self, inst: &AcgInstance) -> AcgResult<()> {
        let id = inst.0.instance_id().ok_or(AcgError::InvalidInstance)?;

        self.runtime
            .block_on(self.client.terminate_instances().instance_ids(id).send())?;

        Ok(())
    }
}

impl FromC for &mut Acg {
    type Target = Arc<AcgClient>;

    fn try_convert(self) -> AcgResult<Self::Target> {
        AcgClient::from_handle(self)
    }
}
