#[cfg(not(feature = "capi"))]
compile_error!("This crate must be built using cargo-c (cargo cbuild, not cargo build)");
