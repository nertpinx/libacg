use std::ffi::CStr;

use crate::errors::{AcgError, AcgResult};
use crate::helpers::*;

pub(crate) trait FromC {
    type Target;

    fn from_c(self) -> Self::Target
    where
        Self: Sized,
    {
        self.try_convert().expect("Translation from C failed")
    }

    fn try_from_c(self, error: *mut *mut AcgError) -> Option<Self::Target>
    where
        Self: Sized,
    {
        let out = self.try_convert();

        if let Err(e) = out {
            if !error.is_null() {
                unsafe {
                    *error = e.to_c();
                }
            }
            return None;
        }

        out.ok()
    }

    fn try_convert(self) -> AcgResult<Self::Target>;
}

impl<'a> FromC for Option<&'a std::ffi::c_char> {
    type Target = &'a str;

    fn try_convert(self) -> AcgResult<Self::Target> {
        self.map(|ptr| unsafe { CStr::from_ptr(ptr) }.to_str())
            .transpose()?
            .ok_or(AcgError::InvalidPointer)
    }
}
