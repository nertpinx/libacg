use crate::helpers::*;
use crate::*;

pub(crate) trait ToCArray {
    type Target;
    type Return;

    fn to_c_array(self, arr: *mut *mut Self::Target) -> Self::Return
    where
        Self: Sized,
    {
        self.try_convert_array(arr)
            .expect("Translation to C failed")
    }

    fn try_to_c_array(
        self,
        arr: *mut *mut Self::Target,
        error: *mut *mut AcgError,
    ) -> Option<Self::Return>
    where
        Self: Sized,
    {
        let out = self.try_convert_array(arr);

        if let Err(e) = out {
            if !error.is_null() {
                unsafe {
                    *error = e.to_c();
                }
            }
            return None;
        }

        out.ok()
    }

    fn try_convert_array(self, arr: *mut *mut Self::Target) -> AcgResult<Self::Return>;
}

impl<T: Opaque> ToCArray for Vec<T> {
    // The list keeps the ownership of each element, so no <T as Opaque>::Target here
    type Target = T;
    type Return = usize;

    fn try_convert_array(mut self, arr: *mut *mut Self::Target) -> AcgResult<Self::Return> {
        if arr.is_null() {
            return Ok(self.len());
        }

        self.shrink_to_fit();
        let (ptr, len, _) = self.into_raw_parts();
        unsafe {
            *arr = ptr;
        }

        return Ok(len);
    }
}

impl<T: ToCArray, E> ToCArray for Result<T, E>
where
    AcgError: From<E>,
{
    type Target = <T as ToCArray>::Target;
    type Return = <T as ToCArray>::Return;

    fn try_convert_array(self, arr: *mut *mut Self::Target) -> AcgResult<Self::Return> {
        self?.try_convert_array(arr)
    }
}
