use crate::helpers::*;
use crate::*;

pub trait Opaque {}

impl Opaque for AcgInstance {}
impl Opaque for AcgError {}
impl Opaque for Acg {}

impl<T: Opaque> ToC for T {
    type Target = *mut T;

    fn try_convert(self) -> AcgResult<Self::Target> {
        Ok(Box::into_raw(Box::new(self)))
    }
}
