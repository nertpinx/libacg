use std::ffi::CString;

use crate::*;

pub(crate) trait ToC {
    type Target;

    fn to_c(self) -> Self::Target
    where
        Self: Sized,
    {
        self.try_convert().expect("Translation to C failed")
    }

    fn try_to_c(self, error: *mut *mut AcgError) -> Option<Self::Target>
    where
        Self: Sized,
    {
        let out = self.try_convert();

        if let Err(e) = out {
            if !error.is_null() {
                unsafe {
                    *error = e.to_c();
                }
            }
            return None;
        }

        out.ok()
    }

    fn try_convert(self) -> AcgResult<Self::Target>;
}

/// Blanket implementations for wrapper structs

impl<T> ToC for Option<T>
where
    T: ToC,
{
    type Target = Option<<T as ToC>::Target>;

    fn try_convert(self) -> AcgResult<Self::Target> {
        self.map(ToC::try_convert).transpose()
    }
}

impl<T, U, V: ToC<Target = U>> ToC for Result<V, T>
where
    AcgError: From<T>,
{
    type Target = U;

    fn try_convert(self) -> AcgResult<Self::Target> {
        self?.try_convert()
    }
}

/// Implementations for standard types

impl ToC for () {
    type Target = std::ffi::c_int;

    fn try_convert(self) -> AcgResult<Self::Target> {
        Ok(0)
    }
}

impl ToC for CString {
    type Target = *mut std::ffi::c_char;

    fn try_convert(self) -> AcgResult<Self::Target> {
        Ok(self.into_raw())
    }
}

impl ToC for &str {
    type Target = *mut std::ffi::c_char;

    fn try_convert(self) -> AcgResult<Self::Target> {
        CString::new(self.as_bytes()).try_convert()
    }
}
