use thiserror::Error;

pub(crate) type AcgResult<T> = Result<T, AcgError>;

#[derive(Debug, Error)]
pub enum AcgError {
    #[error("String conversion error: {0}")]
    ConversionToC(#[from] std::ffi::NulError),

    #[error("String conversion error: {0}")]
    ConversionFromC(#[from] std::str::Utf8Error),

    #[error("No region selected")]
    NoRegion,

    #[error(transparent)]
    IoError(#[from] std::io::Error),

    #[error(transparent)]
    IntError(#[from] std::num::TryFromIntError),

    #[error("SDK error: {0}")]
    SdkError(String),

    #[error("Invalid pointer")]
    InvalidPointer,

    #[error("No such instance")]
    NoSuchInstance,

    #[error("Instance is invalid, could not get a property")]
    InvalidInstance,
}

impl<T, E> From<aws_sdk_ec2::types::SdkError<T, E>> for AcgError {
    fn from(value: aws_sdk_ec2::types::SdkError<T, E>) -> Self {
        Self::SdkError(value.to_string())
    }
}
