#[cfg(test)]
mod tests {
    use crate::capi::*;

    #[test]
    fn free_null() {
        acg_free(std::ptr::null_mut());
        acg_data_free(std::ptr::null_mut());
        acg_error_free(std::ptr::null_mut());
        acg_error_message_free(std::ptr::null_mut());
    }

    #[test]
    fn new_failures() {
        assert_eq!(
            acg_new(&[0xc3, 0x28], std::ptr::null_mut()),
            std::ptr::null_mut()
        );

        let acg = acg_new(std::ptr::null_mut(), std::ptr::null_mut());
        assert_ne!(acg, std::ptr::null_mut());

        acg_free(std::ptr::null_mut());
        acg_error_free(std::ptr::null_mut());
        acg_error_message_free(std::ptr::null_mut());
    }
}
