#![feature(vec_into_raw_parts)]

mod capi_restrict;

#[cfg(feature = "capi")]
mod capi;

mod client;
mod errors;
mod helpers;
mod instance;
mod tests;

pub use crate::client::*;
pub use crate::errors::*;
pub use crate::instance::*;
