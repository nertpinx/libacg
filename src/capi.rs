use pkg_version::*;

use std::ffi::CString;

use crate::errors::*;
use crate::helpers::*;
use crate::*;

#[no_mangle]
pub extern "C" fn acg_version() -> std::ffi::c_ulong {
    pkg_version_major!() * 1000000 + pkg_version_minor!() * 1000 + pkg_version_patch!()
}

#[no_mangle]
pub extern "C" fn acg_free(handle: *mut Acg) {
    if !handle.is_null() {
        std::mem::drop(unsafe { Box::from_raw(handle) });
    }
}

#[no_mangle]
pub extern "C" fn acg_error_free(err: *mut *mut AcgError) {
    if !err.is_null() && !unsafe { *err }.is_null() {
        std::mem::drop(unsafe { Box::from_raw(*err) });
        unsafe {
            *err = std::ptr::null_mut();
        }
    }
}

#[no_mangle]
pub extern "C" fn acg_string_free(string: *mut std::ffi::c_char) {
    if !string.is_null() {
        std::mem::drop(unsafe { CString::from_raw(string) });
    }
}

#[no_mangle]
pub extern "C" fn acg_instance_free(instance: *mut AcgInstance) {
    if !instance.is_null() {
        std::mem::drop(unsafe { Box::from_raw(instance) });
    }
}

#[no_mangle]
pub extern "C" fn acg_error_get_message(err: *mut AcgError) -> *mut std::ffi::c_char {
    let msg = if err.is_null() {
        "No error".to_string()
    } else {
        Box::leak(unsafe { Box::from_raw(err) }).to_string()
    };

    msg.to_c()
}

#[allow(unused)]
macro_rules! c2r_null_mut {
    ($input:expr, $error:ident, $ret:expr) => {
        if $input.is_null() {
            None
        } else {
            let rv = unsafe { $input.as_mut().try_from_c($error) };
            if rv.is_none() {
                return $ret;
            }
            rv
        }
    };
}

macro_rules! c2r_null {
    ($input:expr, $error:ident, $ret:expr) => {
        if $input.is_null() {
            None
        } else {
            let rv = unsafe { $input.as_ref().try_from_c($error) };
            if rv.is_none() {
                return $ret;
            }
            rv
        }
    };
}

macro_rules! c2r_mut {
    ($input:expr, $error:ident, $ret:expr) => {
        match unsafe { $input.as_mut() } {
            None => {
                unsafe {
                    *$error = AcgError::InvalidPointer.to_c();
                }
                return $ret;
            }
            Some(i) => match i.try_from_c($error) {
                None => return $ret,
                Some(v) => v,
            },
        }
    };
}

macro_rules! c2r {
    ($input:expr, $error:ident, $ret:expr) => {
        match unsafe { $input.as_ref().try_from_c($error) } {
            None => return $ret,
            Some(value) => value,
        }
    };
}

#[no_mangle]
pub extern "C" fn acg_new(region: *const std::ffi::c_char, err: *mut *mut AcgError) -> *mut Acg {
    acg_error_free(err);

    let reg = c2r_null!(region, err, std::ptr::null_mut());

    Acg::new(reg).try_to_c(err).unwrap_or(std::ptr::null_mut())
}

#[no_mangle]
pub extern "C" fn acg_get_region(
    handle: *mut Acg,
    err: *mut *mut AcgError,
) -> *mut std::ffi::c_char {
    acg_error_free(err);

    let client = c2r_mut!(handle, err, std::ptr::null_mut());
    let data = client.get_region();

    data.try_to_c(err).unwrap_or(std::ptr::null_mut())
}

#[no_mangle]
pub extern "C" fn acg_get_instances(
    handle: *mut Acg,
    instances: *mut *mut AcgInstance,
    err: *mut *mut AcgError,
) -> usize {
    acg_error_free(err);

    let client = c2r_mut!(handle, err, 0);
    let data = client.get_instances();

    data.try_to_c_array(instances, err).unwrap_or(0)
}

#[no_mangle]
pub extern "C" fn acg_get_instance_by_id(
    handle: *mut Acg,
    id: *const std::ffi::c_char,
    err: *mut *mut AcgError,
) -> *mut AcgInstance {
    acg_error_free(err);

    let client = c2r_mut!(handle, err, std::ptr::null_mut());
    let instance_id = c2r!(id, err, std::ptr::null_mut());
    let data = client.get_instance_by_id(instance_id);

    data.try_to_c(err).unwrap_or(std::ptr::null_mut())
}

#[no_mangle]
pub extern "C" fn acg_instances_get_nth(list: *mut AcgInstance, n: usize) -> *mut AcgInstance {
    if list.is_null() {
        return std::ptr::null_mut();
    }

    unsafe { list.add(n) }
}

#[no_mangle]
pub extern "C" fn acg_instance_free_list(list: *mut AcgInstance, n: usize) {
    if !list.is_null() {
        std::mem::drop(unsafe { Vec::from_raw_parts(list, n, n) });
    }
}

#[no_mangle]
pub extern "C" fn acg_instance_get_name(
    instance: *mut AcgInstance,
    err: *mut *mut AcgError,
) -> *mut std::ffi::c_char {
    acg_error_free(err);

    let instance = c2r_mut!(instance, err, std::ptr::null_mut());

    instance
        .0
        .instance_id()
        .map(|id| id.to_string().try_to_c(err))
        .unwrap_or(Some(std::ptr::null_mut()))
        .unwrap_or(std::ptr::null_mut())
}

#[no_mangle]
pub extern "C" fn acg_instance_get_state(
    instance: *mut AcgInstance,
    err: *mut *mut AcgError,
) -> AcgInstanceState {
    acg_error_free(err);

    let instance = c2r_mut!(instance, err, AcgInstanceState::Unknown);

    instance
        .get_state()
        .try_to_c(err)
        .unwrap_or(AcgInstanceState::Unknown)
}

#[no_mangle]
pub extern "C" fn acg_instance_start(
    handle: *mut Acg,
    instance: *mut AcgInstance,
    err: *mut *mut AcgError,
) -> std::ffi::c_int {
    acg_error_free(err);

    let client = c2r_mut!(handle, err, -1);
    let instance = c2r_mut!(instance, err, -1);

    client.start_instance(&instance).try_to_c(err).unwrap_or(-1)
}

#[no_mangle]
pub extern "C" fn acg_instance_stop(
    handle: *mut Acg,
    instance: *mut AcgInstance,
    err: *mut *mut AcgError,
) -> std::ffi::c_int {
    acg_error_free(err);

    let client = c2r_mut!(handle, err, -1);
    let instance = c2r_mut!(instance, err, -1);

    client.stop_instance(&instance).try_to_c(err).unwrap_or(-1)
}

#[no_mangle]
pub extern "C" fn acg_instance_terminate(
    handle: *mut Acg,
    instance: *mut AcgInstance,
    err: *mut *mut AcgError,
) -> std::ffi::c_int {
    acg_error_free(err);

    let client = c2r_mut!(handle, err, -1);
    let instance = c2r_mut!(instance, err, -1);

    client
        .terminate_instance(&instance)
        .try_to_c(err)
        .unwrap_or(-1)
}
